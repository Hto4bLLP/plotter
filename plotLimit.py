#!/usr/bin/env python
import math
import argparse
import re
import os
from plot_classes import Graph1D, GraphBrazil
from ROOT import *


if __name__ == "__main__":

    f = TFile("/home/net3/jburzyns/VH4b/StatisticalAnalysis/run/ZH_a55a55_4b.root")

    g_lim   = f.Get("expected")
    g_lim1  = f.Get("expected_pm_1sigma")
    g_lim2  = f.Get("expected_pm_2sigma")

    sigma_graphs = [g_lim, g_lim1, g_lim2]

    legs_sig = ["Expected","Expected #pm 1 #sigma", "Expected #pm 2 #sigma" ]
    keys_sig = ["expected","1sigma", "2sigma"]

    GraphBrazil(graphs = sigma_graphs,
           types = keys_sig,
           legends = legs_sig,
           y_title = "95% CL Upper Limit on #font[152]{B}_{#it{H}#rightarrow #it{a}#it{a}}",
           name = "brazil",
           x_title = "#it{a} proper decay length",
           x_units = "m",
           y_min = 0.01,
           y_max = 100,
           x_min = 0.0004,
           x_max = 10,
           log_scale_y = True,
           log_scale_x = True,
           lumi_val = "140",
           hide_lumi = False,
           extra_lines_loc = [0.17,0.775],
           extra_legend_lines = ["m_{a} = 55 GeV","n_{vtx} = 2, n_{bkg} = 1"]
           )
