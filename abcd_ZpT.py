#!/usr/bin/env python
from array import array
from ROOT import *
gROOT.SetBatch(1)

inputDir='/eos/atlas/atlascerngroupdisk/phys-exotics/ueh/VH4b/'
inputFiles=['ZJets.root','ZH_a25a25_4b_ctau10.root']   #"Data" first, followed by signals
#inputFiles=['data.root','ZH_a25a25_4b_ctau10.root']

nDV=1
ZpTBinsLow=range(0,30,5)
ZpTBinsHigh=[30,40,60,100,200,500]

selection='passPresel*totalEventWeight'

#dvSelection='((secVtx_m/secVtx_maxDR)>1) * ((secVtx_m/secVtx_maxDR)<3) * secVtx_passPresel * (secVtx_ntrk>=3)'
dvSelection='((secVtx_m/secVtx_maxDR)>3) * secVtx_passPresel * (secVtx_ntrk>=3)'

######################################################################
    
abcd=TRooABCD('abcd','abcd')

for i in range(len(inputFiles)):
    f=TFile(inputDir+'/'+inputFiles[i])
    t=f.Get("tree")

    sampleName=inputFiles[i].split('.')[0]
    a=TH1F('A_'+sampleName,'',len(ZpTBinsHigh)-1,array('d',ZpTBinsHigh))
    b=TH1F('B_'+sampleName,'',len(ZpTBinsHigh)-1,array('d',ZpTBinsHigh))
    c=TH1F('C_'+sampleName,'',len(ZpTBinsLow)-1,array('d',ZpTBinsLow))
    d=TH1F('D_'+sampleName,'',len(ZpTBinsLow)-1,array('d',ZpTBinsLow))

    t.Draw('Z_pt>>A_'+sampleName,'(%s)*(Sum$(%s)==%i)'%(selection,dvSelection,nDV))
    t.Draw('Z_pt>>C_'+sampleName,'(%s)*(Sum$(%s)==%i)'%(selection,dvSelection,nDV))

    t.Draw('Z_pt>>B_'+sampleName,'(%s)*(Sum$(%s)==%i)'%(selection,dvSelection,0))
    t.Draw('Z_pt>>D_'+sampleName,'(%s)*(Sum$(%s)==%i)'%(selection,dvSelection,0))

    if i==0: 
        if sampleName!='data': abcd.AddData(0,a)
        abcd.AddData(1,b)
        abcd.AddData(2,c)
        abcd.AddData(3,d)
    else:
        abcd.AddSignal(0,a)
        abcd.AddSignal(1,b)
        abcd.AddSignal(2,c)
        abcd.AddSignal(3,d)

abcd.GetParameter("m1").setConstant(False)
result = abcd.Fit()
canvas=gROOT.GetListOfCanvases().FindObject('abcd_postfit')
canvas.SaveAs('abcd_postfit.pdf')
