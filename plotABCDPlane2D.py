#!/usr/bin/env python
import math
import array
import argparse
import re
import os
from plot_classes import Hist1D, Hist2D, HistStack
from ROOT import *

if __name__ == "__main__":


    fM55 = TFile("/data/jburzyns/NtupleReader/ZH_a55a55_4b_ctau10.root")
    fM25 = TFile("/data/jburzyns/NtupleReader/ZH_a25a25_4b_ctau10.root")
    fM35 = TFile("/data/jburzyns/NtupleReader/ZH_a35a35_4b_ctau10.root")
    fBkg = TFile("/data/jburzyns/NtupleReader/ZJets.root")

    bins = array.array('d',[0,30,100000])

    tM55 = fM55.Get("tree")
    tM25 = fM25.Get("tree")
    tM35 = fM35.Get("tree")
    tBkg = fBkg.Get("tree")

    ABCDM55 = TH2F("ABCDM55","",3,0,3,2,bins)
    ABCDM25 = TH2F("ABCDM25","",3,0,3,2,bins)
    ABCDM35 = TH2F("ABCDM35","",3,0,3,2,bins)
    ABCDBkg = TH2F("ABCDBkg","",3,0,3,2,bins)

    tBkg.Draw("Z_pt:Sum$(secVtx_passPresel*(secVtx_m/secVtx_maxDR>3)*(secVtx_ntrk>=3))>>+ABCDBkg","passPresel")
    tM55.Draw("Z_pt:Sum$(secVtx_passPresel*(secVtx_m/secVtx_maxDR>3)*(secVtx_ntrk>=3))>>+ABCDM55","passPresel*totalEventWeight")
    tM25.Draw("Z_pt:Sum$(secVtx_passPresel*(secVtx_m/secVtx_maxDR>3)*(secVtx_ntrk>=3))>>+ABCDM25","passPresel*totalEventWeight")
    tM35.Draw("Z_pt:Sum$(secVtx_passPresel*(secVtx_m/secVtx_maxDR>3)*(secVtx_ntrk>=3))>>+ABCDM35","passPresel*totalEventWeight")
    
    ABCDBkg_6 = TH2F("ABCDBkg_6","",3,0,3,2,0,2)
    C  = ABCDBkg.GetBinContent(1,1)
    Dp = ABCDBkg.GetBinContent(2,1)
    D  = ABCDBkg.GetBinContent(3,1)
    B  = ABCDBkg.GetBinContent(1,2)
    Ap = ABCDBkg.GetBinContent(2,2)
    A  = ABCDBkg.GetBinContent(3,2)

    ABCDBkg_6.SetBinContent(1,1,C)
    ABCDBkg_6.SetBinContent(2,1,Dp)
    ABCDBkg_6.SetBinContent(3,1,D)
    ABCDBkg_6.SetBinContent(1,2,B)
    ABCDBkg_6.SetBinContent(2,2,Ap)
    ABCDBkg_6.SetBinContent(3,2,A)

    ABCDBkg_6.GetYaxis().SetBinLabel(1,"< 30")
    ABCDBkg_6.GetYaxis().SetBinLabel(2,"> 30")

    Hist2D(ABCDM55, 
           name = "ABCDM55", 
           x_title = "n_{vtx}", 
           y_title = "Z p_{T}", 
           y_units = "GeV",
           hide_lumi = False,
           log_scale_z = True,
           lumi_val = "140",
           extra_legend_lines = ["m_{a},c#tau = [55,10]"])
    Hist2D(ABCDM35, 
           name = "ABCDM35", 
           x_title = "n_{vtx}", 
           y_title = "Z p_{T}", 
           y_units = "",
           hide_lumi = False,
           log_scale_z = True,
           lumi_val = "140",
           extra_legend_lines = ["m_{a},c#tau = [35,10]"])
    Hist2D(ABCDM25, 
           name = "ABCDM25", 
           x_title = "n_{vtx}", 
           y_title = "Z p_{T}", 
           y_units = "",
           hide_lumi = False,
           log_scale_z = True,
           lumi_val = "140",
           extra_legend_lines = ["m_{a},c#tau = [25,10]"])
    Hist2D(ABCDBkg_6, 
           name = "ABCDBkg", 
           x_title = "n_{vtx}", 
           y_title = "Z p_{T}", 
           y_units = "GeV",
           hide_lumi = False,
           log_scale_z = False,
           lumi_val = "140",
           extra_legend_lines = ["SHERPA Z+Jets"])
