from ROOT import *
try: import atlas_style
except: print 'WARNING: no atlas_style found'

import os
import math
from sys import argv, exit

from plot_base import *
from plot_util import *

gROOT.SetBatch(kTRUE)
gStyle.SetOptStat(0)
gStyle.SetLineWidth(2)

class HistStack(PlotBase):
    def __init__(self, hists, types, legends, y_axis_type = "Vertices", **kwargs):

        super(HistStack, self).__init__(
                legend_loc = [0.6,0.9,0.9, 0.9 - 0.045*(len(hists)+1) ],
                atlas_loc = [0.17,0.875],
                extra_lines_loc = [0.17,0.775],
                **kwargs)

        self.hists = hists
        self.hs = THStack("hs","")


        for h, t in zip(self.hists, types):
            if (self.rebin != None):
                h.Rebin(self.rebin)

            self.leg.AddEntry(h, legends[ self.hists.index(h)  ], 'f')

            format_for_drawing_stack(h, t)
#            h.GetYaxis().SetMaxDigits(3);
            self.hs.Add(h)


        self.pad_empty_space(self.hists)
        pad1 = self.canvas.cd(1)
        format_simple_pad(pad1)
        pad1.cd()
        if (self.log_scale_y):
            pad1.SetLogy()
        if (self.log_scale_x):
            pad1.SetLogx()

        self.hs.Draw("hist")
        self.set_x_axis_bounds(self.hs)
        self.set_y_min(self.hs)
        self.set_y_max(self.hs)
        self.set_titles(self.hs, y_axis_type)
        hs.GetXaxis().SetTitleOffset(1.2);
        hs.GetYaxis().SetTitleOffset(1.2);
        hs.GetXaxis().SetTitleSize(.05);
        hs.GetYaxis().SetTitleSize(.05);
        hs.GetXaxis().SetLabelSize(.05);
        hs.GetYaxis().SetLabelSize(.05);
        hs.GetXaxis().SetLabelOffset(0.01);

        self.print_to_file(self.name + ".pdf")
        pad1.Close()


class Graph1D(PlotBase):
    def __init__(self, graphs, types, legends, **kwargs):

        super(Graph1D, self).__init__(
                legend_loc = [0.5,0.9,0.9, 0.9 - 0.045*(len(graphs)+1) ],
                atlas_loc = [0.17,0.875],
                #extra_lines_loc = [0.17,0.775],
                **kwargs)

        self.graphs = graphs
        h = TH2F("h1","Cross-Section Limit",1000,self.x_min,self.x_max,1000,self.y_min,self.y_max)
        format_for_drawing(h, "data")
        h.GetYaxis().SetTitle(self.y_title)
        h.GetXaxis().SetTitle(self.x_title + " [" + self.x_units + "]")

        h.Draw("");
        h.SetMaximum(1.0);
        h.SetMinimum(0.0);

        for g, t in zip(self.graphs, types):
            self.leg.AddEntry(g, legends[ self.graphs.index(g)  ], 'l')

            self.set_x_axis_bounds(g)
            self.set_y_min(g)
            self.set_y_max(g)
            self.set_titles_graph(g)
            format_for_drawing(g, t)

        pad1 = self.canvas.cd(1)
        format_simple_pad(pad1)
        pad1.cd()
        if (self.log_scale_y):
            pad1.SetLogy()
        if (self.log_scale_x):
            pad1.SetLogx()

        draw_graphs(self.graphs, "C", types)
        pad1.Update()
        pad1.RedrawAxis()

        self.print_to_file(self.name + ".pdf")
        pad1.Close()


class GraphBrazil(PlotBase):
    def __init__(self, graphs, types, legends, **kwargs):

        super(GraphBrazil, self).__init__(
                legend_loc = [0.5,0.9,0.9, 0.9 - 0.045*(len(graphs)+1) ],
                atlas_loc = [0.17,0.875],
                #extra_lines_loc = [0.17,0.775],
                **kwargs)

        self.graphs = graphs
        h = TH2F("h1","Cross-Section Limit",1000,self.x_min,self.x_max,1000,self.y_min,self.y_max)
        format_for_drawing(h, "data")
        h.GetYaxis().SetTitle(self.y_title)
        h.GetXaxis().SetTitle(self.x_title + " [" + self.x_units + "]")

        h.Draw("");
        h.SetMaximum(1.0);
        h.SetMinimum(0.0);

        for g, t in zip(self.graphs, types):
            self.leg.AddEntry(g, legends[ self.graphs.index(g)  ], 'l') if "sigma" not in t else self.leg.AddEntry(g, legends[ self.graphs.index(g)  ], 'lf')

            self.set_x_axis_bounds(g)
            self.set_y_min(g)
            self.set_y_max(g)
            self.set_titles_graph(g)
            format_for_drawing(g, t)

        pad1 = self.canvas.cd(1)
        format_simple_pad(pad1)
        pad1.cd()
        if (self.log_scale_y):
            pad1.SetLogy()
        if (self.log_scale_x):
            pad1.SetLogx()

        graphs[types.index("2sigma")].SetFillColor(kYellow)
        graphs[types.index("2sigma")].SetLineWidth(0)
        graphs[types.index("2sigma")].Draw("E3")
        graphs[types.index("1sigma")].SetFillColor(kGreen)
        graphs[types.index("1sigma")].SetLineWidth(0)
        graphs[types.index("1sigma")].Draw("E3")
        graphs[types.index("expected")].SetLineWidth(2)
        graphs[types.index("expected")].SetLineStyle(7)
        graphs[types.index("expected")].Draw("C")


        pad1.Update()
        pad1.RedrawAxis()

        self.print_to_file(self.name + ".pdf")
        pad1.Close()

class Hist1DRatio(PlotBase):
    def __init__(self, num, denom, types, legends, y_axis_type = "Vertices", **kwargs):

        super(Hist1DRatio, self).__init__(
                legend_loc = [0.5,0.9,0.9, 0.9],
                atlas_loc = [0.17,0.875],
                extra_lines_loc = [0.17,0.775],
                **kwargs)

        self.num = num
        self.denom = denom

        self.hists = [num,denom]

        y_min = 10000000
        y_max = -1
        for h, t in zip(self.hists, types):
            if (self.rebin != None):
                h.Rebin(self.rebin)
            if(self.norm == True):
                h.Scale(1.0/h.Integral())

            self.set_x_axis_bounds(h)
            self.set_y_min(h)
            self.set_y_max(h)
            self.set_titles(h, y_axis_type)
            format_for_drawing(h, t)
            h.GetYaxis().SetMaxDigits(3);

        self.pad_empty_space(self.hists)

        ratio = num.Clone("ratio")
        ratio.Divide(denom)
        ratio.SetMaximum(2)
        ratio.SetMinimum(0)
        ratio.GetYaxis().SetTitle("Data/MC")
        ratio.SetMarkerColor(kBlack)
        ratio.SetLineColor(kBlack)

        pad1, pad2 = format_2pads_for_ratio()

        pad1.Draw()
        pad2.Draw()

        pad1.cd()
        if (self.log_scale_y):
            pad1.SetLogy()

        denom.Draw("hist")
        denom.SetFillColor(kRed)
        #num.Draw("hist same")
        num.Draw("PE same")

        pad2.cd()
        ratio.Draw("PE")
        line = TLine(ratio.GetXaxis().GetXmin(),1.0,ratio.GetXaxis().GetXmax(),1.0)
        line.SetLineColor(kBlack)
        line.SetLineWidth(1)
        line.SetLineStyle(2)
        line.Draw("same")

        self.canvas.Update()
        self.canvas.Modified()

        self.print_to_file(self.name + "ratio.pdf")

class Hist1D(PlotBase):
    def __init__(self, hists, types, legends, y_axis_type = "Vertices", **kwargs):

        super(Hist1D, self).__init__(
                legend_loc = [0.5,0.9,0.9, 0.9 - 0.045*(len(hists)+1) ],
                atlas_loc = [0.17,0.875],
                #extra_lines_loc = [0.17,0.775],
                **kwargs)

        self.hists = hists

        y_min = 10000000
        y_max = -1
        for h, t in zip(self.hists, types):
            if (self.rebin != None):
                h.Rebin(self.rebin)
            if "LRT" not in h.GetName():
              self.leg.AddEntry(h, legends[ self.hists.index(h)  ], 'lp')

            if(self.norm == True):
                h.Scale(1.0/h.Integral())

            self.set_x_axis_bounds(h)
            self.set_y_min(h)
            self.set_y_max(h)
            self.set_titles(h, y_axis_type)
            format_for_drawing(h, t)
            if "LRT" in h.GetName():
              h.SetMarkerStyle(h.GetMarkerStyle()+4)
            h.GetYaxis().SetMaxDigits(3);


        self.pad_empty_space(self.hists)
        pad1 = self.canvas.cd(1)
        format_simple_pad(pad1)
        pad1.cd()
        if (self.log_scale_y):
            pad1.SetLogy()
        if (self.log_scale_x):
            pad1.SetLogx()


        draw_hists(self.hists, "PE hist", types)
        if "_r" in self.name:
            self.draw_material_layers()

        self.print_to_file(self.name + ".pdf")
        pad1.Close()

    def draw_material_layers(self):
        coords = [33.5,50.5,88.5,122.5,299.0]
        for coord in coords:
            line = TLine()
            line.SetLineColor(kRed)
            line.SetLineWidth(2)
            line.SetLineStyle(8)
            line.DrawLine(coord,0,coord,self.y_max * 1.0/self.empty_scale)
            if(coords.index(coord) == 0):
                self.leg.AddEntry(line, "Material Layers", "l")
        
class Hist2D(PlotBase):
    def __init__(self, hist, **kwargs):

        super(Hist2D, self).__init__(
                legend_loc = [0.6,0.9,0.85, 0.9 ],
                atlas_loc = [0.175,0.875],
                extra_lines_loc = [0.5,0.875],
                **kwargs)

        self.hist = hist

        self.set_x_axis_bounds(self.hist)
        self.set_y_axis_bounds(self.hist)
        self.set_z_min(self.hist)
        self.set_z_max(self.hist)

        hist.GetZaxis().SetMaxDigits(3);
        self.set_titles(self.hist,"")

        pad1 = self.canvas.cd(1)
        format_for_drawing(hist)
        format_simple_pad_2D(pad1)
        pad1.cd()
        if (self.log_scale_y):
            pad1.SetLogy()
        if (self.log_scale_x):
            pad1.SetLogx()
        if (self.log_scale_z):
            pad1.SetLogz()

        self.hist.Draw("colz TEXT0")
#        py = self.hist.ProfileX("py",0,4,"o")
#        py.Draw("same")

        self.print_to_file(self.name + ".pdf")
        pad1.Close()

