#!/usr/bin/env python
import math
import argparse
import re
import os
from array import array
from plot_classes import Hist1D, Hist2D, HistStack, Hist1DRatio
from ROOT import *
import pdb
from collections import OrderedDict

inputDir='..'
inputFiles=['data.root','ZJets.root']  #put data first

selections=['passPresel'] #,'Sum$(secVtx_passPresel * (secVtx_ntrk>=3) * ((secVtx_m/secVtx_maxDR)>3))==0']

weight='totalEventWeight'

############################################################################################

files = OrderedDict()	    
trees = OrderedDict()	

for file in inputFiles:
	f=inputDir+'/'+file
	key = file.split('.')[0]
	files[key] = TFile(f)
	trees[key] = files[key].Get("tree")

############################################################################################

class Plot:
	def __init__(self,variable,selection,bins=None,nBins=100,xMin=0,xMax=100,xTitle='',yTitle='Events',yLog=True):
		self.variable=variable; self.selection=selection; self.bins=bins; self.nBins=nBins; self.xMin=xMin; self.xMax=xMax; self.xTitle=xTitle; self.yTitle=yTitle; self.yLog=yLog;

		self.name=self.variable+'_'+self.selection
		self.name=self.name.replace('/','OVER').replace('*','TIMES').replace('-','MINUS').replace('+','PLUS').replace('>=','GEQ').replace('<=','LEQ').replace('>','GT').replace('<','LT').replace('Sum$','SUM').replace('==','EQ').replace(' ','').replace('(','').replace(')','')

		if self.bins:
			self.nBins=len(self.bins)-1
			self.xMin=self.bins[0]
			self.xMax=self.bins[-1]
		else:
			self.bins=[self.xMin+(i*(float(self.xMax-self.xMin)/self.nBins)) for i in range(self.nBins+1)]
		self.bins=array('f',self.bins)

		# - - - -  - - - -  - - - -  - - - -  - - - -  - - - -  - - - -  - - - -  - - - -  - - - - 

		for key in trees:
			t=trees[key]
			h = TH1F("%s_%s"%(key,self.name),";%s;%s"%(self.xTitle,self.yTitle), self.nBins, self.bins)
			h.SetDirectory(gDirectory)
			#print 'DRAWING:',self.variable+">>"+h.GetName(),self.selection+'*(%s)'%weight
			t.Draw(self.variable+">>"+h.GetName(),self.selection+'*(%s)'%weight)
			if 'data' in key: 
				data=h
			else:
				h.Scale(0.412)
				bkgd=h
			 
		Hist1DRatio( num = data,
					 denom = bkgd,
					 types = trees.keys(),
					 legends = [legends[key] for key in trees.keys()],
					 name = self.name,
					 x_title = self.xTitle,
					 #x_units = labels[v][1],
					 #y_min = args.y_min,
					 log_scale_y = self.yLog,
					 y_axis_type = self.yTitle,
					 lumi_val = str(140*0.412),
					 hide_lumi = False,
					 #norm = args.norm,
					 #extra_lines_loc = [0.5,0.55],
					 #extra_legend_lines = leg_lines
			)
		
############################################################################################

legends = {
	"data" : "data",
	"ZJets": "SHERPA Z+jets",
	"WH_a55a55_4b_ctau1"  : "m_{a}, c#tau = [55,1]",
	"WH_a55a55_4b_ctau10" : "m_{a}, c#tau = [55,10]",
	"WH_a55a55_4b_ctau100": "m_{a}, c#tau = [55,100]",
	"WH_a15a15_4b_ctau1"  : "m_{a}, c#tau = [15,1]",
	"WH_a15a15_4b_ctau10" : "m_{a}, c#tau = [15,10]",
	"WH_a15a15_4b_ctau100": "m_{a}, c#tau = [15,100]",
	"ZH_a55a55_4b_ctau10" : "m_{a}, c#tau = [55,10]",
	"ZH_a35a35_4b_ctau10" : "m_{a}, c#tau = [35,10]",
	"ZH_a25a25_4b_ctau10" : "m_{a}, c#tau = [25,10]",
	"ZH_a15a15_4b_ctau10" : "m_{a}, c#tau = [15,10]",
}

############################################################################################

if __name__ == "__main__":
   
	for s in selections:
		Plot('Z_pt',              s,bins=range(0,100,5)+range(100,150,10)+[150,250,500],xTitle='Z p_{T} [GeV]')
		Plot('Z_m' ,              s,nBins=24,xMin=65,xMax=125,                    xTitle='Z mass [GeV]')	
		Plot('Z_eta',             s,nBins=20,xMin=-5,xMax=5,                      xTitle='Z #eta',yLog=False)
		Plot('l1_pt',             s,nBins=30,xMin=0 ,xMax=100,                    xTitle='l_{1} p_{T} [GeV]')
		Plot('l2_pt',             s,nBins=30,xMin=0 ,xMax=100,                    xTitle='l_{2} p_{T} [GeV]')
		Plot('Ht',                s,nBins=30,xMin=0 ,xMax=500,                    xTitle='H_{T} [GeV]')
		Plot('(l1_pt-l2_pt)/Z_pt',s,nBins=20,xMin=0 ,xMax=1,                      xTitle='p_{T} imbalance',yLog=False)
		Plot('j1_pt',             s,nBins=30,xMin=0 ,xMax=100,                    xTitle='j_{1} p_{T} [GeV]')
		Plot('j2_pt',             s,nBins=30,xMin=0 ,xMax=100,                    xTitle='j_{2} p_{T} [GeV]')


			

