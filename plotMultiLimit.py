#!/usr/bin/env python
import math
import argparse
import re
import os
from plot_classes import Graph1D, GraphBrazil
from ROOT import *


if __name__ == "__main__":

    f_55 = TFile("ZH_a55a55_4b.root")
    f_35 = TFile("ZH_a35a35_4b.root")
    f_25 = TFile("ZH_a25a25_4b.root")

    g_55 = f_55.Get("expected")
    g_35 = f_35.Get("expected")
    g_25 = f_25.Get("expected")

    lim_graphs = [g_55, g_35, g_25]

    legs_lim = ["m_{a} = 55 GeV", "m_{a} = 35 GeV", "m_{a} = 25 GeV"]
    keys_lim = ["ZH_a55a55_4b_ctau10","ZH_a35a35_4b_ctau10", "ZH_a25a25_4b_ctau10"]

    Graph1D(graphs = lim_graphs,
           types = keys_lim,
           legends = legs_lim,
           y_title = "95% CL Upper Limit on #font[152]{B}_{#it{H}#rightarrow #it{a}#it{a}}",
           name = "limits",
           x_title = "#it{a} proper decay length",
           x_units = "m",
           y_min = 0.01,
           y_max = 10,
           x_min = 0.0005,
           x_max = 5,
           log_scale_y = True,
           log_scale_x = True,
           lumi_val = "140",
           hide_lumi = False,
           extra_lines_loc = [0.17,0.775],
           extra_legend_lines = ["n_{vtx} = 2, n_{bkg} = 1"]
           )
