from plot_classes import Hist1D
from ROOT import TFile


ZJets = TFile("/data/jburzyns/NtupleReader/ZJets.root")
ZHa55 = TFile("/data/jburzyns/NtupleReader/ZH_trees_CR/ZH_a55a55_4b_ctau10.root")
ZHa35 = TFile("/data/jburzyns/NtupleReader/ZH_trees_CR/ZH_a35a35_4b_ctau10.root")
ZHa25 = TFile("/data/jburzyns/NtupleReader/ZH_trees_CR/ZH_a25a25_4b_ctau10.root")
ZHa15 = TFile("/data/jburzyns/NtupleReader/ZH_trees_CR/ZH_a15a15_4b_ctau10.root")

h_ZJets = ZJets.Get("cutflow")
h_ZHa55 = ZHa55.Get("cutflow")
h_ZHa35 = ZHa35.Get("cutflow")
h_ZHa25 = ZHa25.Get("cutflow")
h_ZHa15 = ZHa15.Get("cutflow")

histos = [h_ZJets, h_ZHa55, h_ZHa35, h_ZHa25, h_ZHa15]

for hist in histos:
  hist.Scale(1.0/hist.GetBinContent(1))

keys = ["ZJets", "ZH_a55a55_4b_ctau10","ZH_a35a35_4b_ctau10", "ZH_a25a25_4b_ctau10", "ZH_a15a15_4b_ctau10"]

legends = {
  "ZJets" : "SHERPA Z+jets",
  "WH_a55a55_4b_ctau1" : "m_{a}, c#tau = [55,1]",
  "WH_a55a55_4b_ctau10" : "m_{a}, c#tau = [55,10]",
  "WH_a55a55_4b_ctau100" : "m_{a}, c#tau = [55,100]",
  "WH_a15a15_4b_ctau1" : "m_{a}, c#tau = [15,1]",
  "WH_a15a15_4b_ctau10" : "m_{a}, c#tau = [15,10]",
  "WH_a15a15_4b_ctau100" : "m_{a}, c#tau = [15,100]",
  "ZH_a55a55_4b_ctau10" : "m_{a}, c#tau = [55,10]",
  "ZH_a35a35_4b_ctau10" : "m_{a}, c#tau = [35,10]",
  "ZH_a25a25_4b_ctau10" : "m_{a}, c#tau = [25,10]",
  "ZH_a15a15_4b_ctau10" : "m_{a}, c#tau = [15,10]",
}


legs = []
for key in keys:
  legs.append(legends[key])

Hist1D( hists = histos,
        types = keys,
        legends = legs,
        name = "cutflow",
        x_title = "",
        x_units = "",
        y_min = 0.0000001,
        log_scale_y = True,
        y_axis_type = "Events",
        lumi_val = "140",
        hide_lumi = False,
        norm = False,
        extra_lines_loc = [0.45,0.55],
        extra_legend_lines = "" )
