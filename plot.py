#!/usr/bin/env python
import math
import argparse
import re
import os
from plot_classes import Hist1D, Hist2D, HistStack, Hist1DRatio
from ROOT import *

variables=['Z_m','Z_pt']
selections=['1','Sum$(((secVtx_m/secVtx_maxDR)>1) * ((secVtx_m/secVtx_maxDR)<3) * secVtx_passPresel * (secVtx_ntrk>=3))==0']
#selections=['totalEventWeight*passPresel * (Sum$(((secVtx_m/secVtx_maxDR)>1) * ((secVtx_m/secVtx_maxDR)<3) * secVtx_passPresel * (secVtx_ntrk>=3))==0)']

bins = {
  "nSecVtx_signal": [5,0,5],
  "nSecVtx": [7,0,7],
  "nSecVtx_presel": [5,0,5],
  "nSecVtx_signal_loose": [5,0,5],
  "nSecVtx_signal_tight": [5,0,5],
  "secVtx_mult": [7,0,7],
  "secVtx_r": [60,0,300],
  "secVtx_ntrk": [13,2,15],
  "secVtx_nSiHits": [30,0,150],
  "secVtx_m": [50,0,50],
  "secVtx_minOneTrackRemovedMass": [50,0,50],
  "secVtx_m/secVtx_maxDR": [50,0,50],
  "secVtx_kerasScore": [20,0,1],
  "mjj" : [100,0,1000],
  "mjjj" : [100,0,1000],
  "mjjjj" : [100,0,1000],
  "dijet_dR" : [50,0,5],
  "dijet_dR3" : [50,0,5],
  "dijet_dR4" : [50,0,5],
  "nJets" : [10,0,10],
  "jet_chf" : [100,0,1],
  "jet_lep_dR" : [50,0,5],
  "j1_pt" : [48,20,500],
  "j2_pt" : [38,20,400],
  "l1_pt" : [48,20,500],
  "Z_pt" : [50,0,500],
  "l2_pt" : [38,20,400],
  "Z_m" : [30,50,200],
  "BDTweight" : [50,-1,1],
  "Lep_JetMinCHF_DeltaPhi" : [32,0,3.2],
  "Lep_SumLeadingJets_DeltaPhi" : [32,0,3.2],
  "minCHFJet_phi" : [64,-3.2,3.2],
  "Ht" : [100,0,1000],
  "Lep_Jet_MinDeltaPhi" : [32,0,3.2],
  "lep_pt_imbalance" : [25,0,1],
  "z_jets_dPhi" : [32,0,3.2],
}

labels = {
  "secVtx_mult": ["Vertex multiplicity",""],
  "nSecVtx": ["Vertex multiplicity",""],
  "nSecVtx_presel": ["Vertex multiplicity",""],
  "nSecVtx_signal": ["Vertex multiplicity",""],
  "nSecVtx_signal_loose": ["Vertex multiplicity",""],
  "nSecVtx_signal_tight": ["Vertex multiplicity",""],
  "secVtx_r": ["L_{xy}","mm"],
  "secVtx_ntrk": ["n_{trk}",""],
  "secVtx_nSiHits": ["Number of Si Hits",""],
  "secVtx_m": ["mass","GeV"],
  "secVtx_minOneTrackRemovedMass": ["minOneTrackRemovedMass","GeV"],
  "secVtx_kerasScore": ["Keras score",""],
  "mjj" : ["m_{jj}","GeV"],
  "mjjj" : ["m_{jjj}","GeV"],
  "mjjjj" : ["m_{jjjj}","GeV"],
  "dijet_dR" : ["#DeltaR_{jj}",""],
  "dijet_dR3" : ["#DeltaR_{avg}",""],
  "dijet_dR4" : ["#DeltaR_{avg}",""],
  "nJets" : ["n_{jet}",""],
  "jet_chf" : ["CHF",""],
  "jet_lep_dR" : ["#DeltaR_{jet,lep}",""],
  "j1_pt" : ["Leading jet p_{T}","GeV"],
  "j2_pt" : ["Subleading jet p_{T}","GeV"],
  "l1_pt" : ["Leading lepton p_{T}","GeV"],
  "l2_pt" : ["Subleading lepton p_{T}","GeV"],
  "Z_pt" : ["Z p_{T}","GeV"],
  "Z_m" : ["Z mass","GeV"],
  "BDTweight" : ["BDT score",""],
  "Lep_JetMinCHF_DeltaPhi" : ["Lep_JetMinCHF_DeltaPhi",""],
  "Lep_SumLeadingJets_DeltaPhi" : ["#Delta#phi(l,jj)",""],
  "minCHFJet_phi" : ["minCHFJet_phi",""],
  "Ht": ["Ht","GeV"],
  "Lep_Jet_MinDeltaPhi" : ["Lep_Jet_MinDeltaPhi",""],
  "lep_pt_imbalance" : ["p_{T} imbalance",""],
  "z_jets_dPhi" : ["|#Delta#phi(Z,jj)|",""],
}

legends = {
  "data" : "data",
  "ZJets" : "SHERPA Z+jets",
  "WH_a55a55_4b_ctau1" : "m_{a}, c#tau = [55,1]",
  "WH_a55a55_4b_ctau10" : "m_{a}, c#tau = [55,10]",
  "WH_a55a55_4b_ctau100" : "m_{a}, c#tau = [55,100]",
  "WH_a15a15_4b_ctau1" : "m_{a}, c#tau = [15,1]",
  "WH_a15a15_4b_ctau10" : "m_{a}, c#tau = [15,10]",
  "WH_a15a15_4b_ctau100" : "m_{a}, c#tau = [15,100]",
  "ZH_a55a55_4b_ctau10" : "m_{a}, c#tau = [55,10]",
  "ZH_a35a35_4b_ctau10" : "m_{a}, c#tau = [35,10]",
  "ZH_a25a25_4b_ctau10" : "m_{a}, c#tau = [25,10]",
  "ZH_a15a15_4b_ctau10" : "m_{a}, c#tau = [15,10]",
}

def parseCutString(cutString):
    legend_lines = []

    cutList = cutString.split('*')

    eventCuts = "Event Preselection"
    vertexCuts = "n_{trk} #geq 3, m\' > 3 GeV"
    for cut in cutList:
        cut = cut.replace(')',"")
        if "_ntrk" in cut:
          vertexCuts += ", n_{trk} #geq " + cut.split('=')[-1] if vertexCuts else "n_{trk} #geq " + cut.split('=')[-1]
        elif "_maxDR" in cut:
          vertexCuts += ", m\' > " + cut.split('>')[-1] if vertexCuts else "m\' > " + cut.split('>')[-1]
        elif "_m" in cut:
          vertexCuts += ", m > " + cut.split('>')[-1] if vertexCuts else "m > " + cut.split('>')[-1]
        elif "_keras" in cut:
          vertexCuts += ", Keras score > " + cut.split('>')[-1] if vertexCuts else "Keras score > " + cut.split('>')[-1]
        elif "_passPresel" in cut:
          vertexCuts += ", Vertex Preselection" if vertexCuts else "Vertex Preselection" 

    if eventCuts:
        legend_lines.append(eventCuts)
    if vertexCuts:
        legend_lines.append(vertexCuts)
    return legend_lines

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--files", nargs='+', required=True, help="list of files to plot" )
    #parser.add_argument("--var", required=True, type=str,help="variable to plot" )
    #parser.add_argument("--cutString", type=str, default="", help="optional cut string to apply during TTree::Draw()")
    parser.add_argument("--log", action='store_true', help="log scale plot")
    parser.add_argument("--norm", action='store_true', help="normalize")
    parser.add_argument("--y_min", type=float, help="y minimum")

    args = parser.parse_args()

    files = {}
    trees = {}

    for f in args.files:
        key = os.path.basename(f).split('.')[0]
        files[key] = TFile(f)
        trees[key] = TTree()
        files[key].GetObject("tree",trees[key])

    for v in variables:
        for s in selections:
        
            histos = []
            hist_dict = {}
            keys = []
            legs = []
            for key in trees:
                keys.append(key)
                h = TH1F("h_"+v+"_"+key,"", bins[v][0], bins[v][1], bins[v][2])
                histos.append(h)
                hist_dict[key] = h
                legs.append(legends[key])

            for key in trees:
                t = trees[key]
                t.Draw(v + ">>+h_"+v+"_"+key,s+"*totalEventWeight*passPresel" if s else "totalEventWeight*passPresel")

            # compute S/B
            cutString = parseCutString(s)
            leg_lines = cutString

            #pdb.set_trace()
            Hist1DRatio( num = histos[0],
                         denom = histos[1],
                         types = keys,
                         legends = legs,
                         name = v+''.join(e for e in s if e.isalnum()),
                         x_title = labels[v][0],
                         x_units = labels[v][1],
                         y_min = args.y_min,
                         log_scale_y = args.log,
                         y_axis_type = "Events" if not "secVtx" in v else "Vertices",
                         lumi_val = "140",
                         hide_lumi = False,
                         norm = args.norm,
                         #extra_lines_loc = [0.5,0.55],
                         extra_legend_lines = leg_lines
            )

